<?php

/**
 * @file
 * Contains \Drupal\sloggen\Form\GenerateSlogitemsForm.
 */

namespace Drupal\sloggen\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sloggen\SlogGen;
use Drupal\slogxt\SlogXt;

/**
 * Confirmation form for generating slogitems.
 */
class GenerateSlogitemsForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sloggen_generate_slogitems';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Generate slogitems');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $args = ['%path' => $this->menuterm->pathLabel()];
    return $this->t('Generate slogitems for menu term: %path', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $args = ['slogtx_mt' => $this->menuterm_id];
    return new Url('entity.slogtx_mt.overview', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Generate');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $menuterm = \Drupal::request()->get('slogtx_mt');
    if ($menuterm && $menuterm->isValidTerm()) {
      $this->menuterm = $menuterm;
      $this->menuterm_id = $menuterm->id();
      $config = $this->config('sloggen.settings');

      $slogxt_data = & $form_state->get('slogxt');
      if (!empty($slogxt_data)) {
        $slogxt_data['menu_tid'] = $this->menuterm_id;
      }

      $form['genopts'] = [
          '#type' => 'details',
          '#id' => 'detail-rootterm',
          '#title' => $this->t('Generate options'),
          '#open' => TRUE,
          '#tree' => FALSE,
      ];

      $mt_number = SlogGen::configValue($config, 'sloggen_mt_number', SlogGen::DEFAULT_MT_NUMBER);
      $mt_range = SlogGen::configValue($config, 'sloggen_mt_range', SlogGen::DEFAULT_MT_RANGE);
      $form['genopts']['sloggen_mt_number'] = [
          '#type' => 'textfield',
          '#title' => t('How many slogitems would you like to generate?'),
          '#description' => t('Maximum number: %max elements', ['%max' => SlogGen::MT_NUMBER_MAX]),
          '#default_value' => $mt_number,
          '#size' => 10,
      ];
      // time range for field 'created' in slogitem table (SlogItem entity)
      $form['genopts']['sloggen_mt_range'] = [
          '#type' => 'select',
          '#title' => t('How far back in time should the slogitems be dated?'),
          '#description' => t('Element creation dates will be distributed randomly from the current time, back to the selected time.'),
          '#options' => SlogGen::optionsTimeAgo(),
          '#default_value' => $mt_range,
      ];
      $toolbar = $menuterm->getVocabulary()->getToolbar();
      $is_sys_tb = $toolbar->isSysToolbar();
      if ($is_sys_tb) {
        $form['genopts']['redirect_elements'] = [
            '#type' => 'checkbox',
            '#title' => t('Redirect elements.'),
            '#description' => t('Create slogitem redirect element, which redirect to other slogitem elements with node target.'),
            '#default_value' => $is_sys_tb,
        ];
      }
      else {
        $form['genopts']['node_type'] = [
            '#type' => 'select',
            '#title' => t('What node type to use'),
            '#options' => SlogGen::optionsNodeTypes(),
            '#description' => t('You may decide to use only nodes of a certain type. This has no effect for redirects.'),
            '#default_value' => '0',
        ];
      }
      $form['genopts']['kill'] = [
          '#type' => 'checkbox',
          '#title' => t('Clear before generating'),
          '#description' => t('Deletes all existing elements before generating new.'),
          '#default_value' => FALSE,
      ];

      return parent::buildForm($form, $form_state);
    }
    else {
      $path = $menuterm ? $menuterm->pathLabel() : 'undefined';
      $args = ['%path' => $path];
      $msg = $this->t("This is no valid menu term. </br>Path: %path.", $args);
      \Drupal::messenger()->addError($msg);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $redirect_elements = !empty($values['redirect_elements']) ? (boolean) $values['redirect_elements'] : FALSE;
    if (!$redirect_elements) {
      $node_ids = SlogGen::getAvailableNodeIds($values['node_type']);
      if (count($node_ids) < 100) {
        $err_msg = t('There are not enough node items to generate slog items.');
        $form_state->setErrorByName('node_type', $err_msg);
      }
    }

//    $names = ['sloggen_mt_number', 'sloggen_mt_maxstatus'];
    $names = ['sloggen_mt_number'];
    foreach ($names as $field_name) {
      $val = (integer) $values[$field_name];
      switch ($field_name) {
        case 'sloggen_mt_number':
          if ($val < 1 || $val > SlogGen::MT_NUMBER_MAX) {
            $err_msg = t('Number: not a valid value.');
            $form_state->setErrorByName($field_name, $err_msg);
          }
          break;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());

    $generated = FALSE;
    if (isset($this->menuterm)) {
      $values = $form_state->getValues();
      $data = [
          'selected_ids' => [$this->menuterm_id],
          'kill' => (boolean) $values['kill'],
          'number' => $values['sloggen_mt_number'],
          'range' => $values['sloggen_mt_range'],
          'redirect_elements' => !empty($values['redirect_elements']) ? (boolean) $values['redirect_elements'] : FALSE,
          'node_type' => $values['node_type'],
      ];
      $generated = SlogGen::generateSlogItems($data);
    }

    // message/log
    $args = [
        '%path' => isset($this->menuterm) ? $this->menuterm->pathLabel() : 'undefined',
        '%tid' => $this->menuterm_id,
    ];
    if ($generated) {
      $msg = $this->t('Slogitems have been generated for menu term: (%tid) %path.', $args);
      \Drupal::messenger()->addStatus($msg);
      SlogXt::logger('sxt_slogitem')->notice($msg);
    }
    else {
      $msg = $this->t('Slogitems have NOT been generated for menu term: (%tid) %path.', $args);
      \Drupal::messenger()->addError($msg);
      SlogXt::logger('sxt_slogitem')->error($msg);
    }
  }

}
