<?php

/**
 * @file
 * Contains \Drupal\sloggen\Form\SloggenSettingsForm.
 */

namespace Drupal\sloggen\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sloggen\SlogGen;

/**
 * Slog Generate settings form.
 */
class SloggenSettingsForm extends ConfigFormBase {

  public function getFormId() {
    return 'sloggen_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['sloggen.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('sloggen.settings');

    $integrate = (boolean) (SlogGen::configValue($config, 'sloggen_integrate', FALSE));
    $this->integrate_old = $integrate;
    $form['sloggen_integrate'] = [
        '#type' => 'checkbox',
        '#title' => 'Integrate (!!! drush cache-rebuild)',
        '#description' => 'Integrate Slog Generate, i.g. in tx root terms and tx menu terms.</br>'
        . 'Helpful for developing. !!! Do not forget to rebuild the cache (drush cache-rebuild) !!!',
        '#default_value' => $integrate,
    ];

    $form['rootterm'] = [
        '#type' => 'details',
        '#id' => 'detail-rootterm',
        '#title' => $this->t('Root term defaults'),
        '#open' => TRUE,
        '#tree' => FALSE,
    ];

    $rt_number = SlogGen::configValue($config, 'sloggen_rt_number', SlogGen::DEFAULT_RT_NUMBER);
    $rt_level = SlogGen::configValue($config, 'sloggen_rt_level', SlogGen::DEFAULT_RT_LEVEL);
    $form['rootterm']['sloggen_rt_number'] = [
        '#type' => 'textfield',
        '#title' => t('How many menu terms would you like to generate per level?'),
        '#description' => t('Maximum number: %max elements', ['%max' => SlogGen::RT_NUMBER_MAX]),
        '#default_value' => $rt_number,
        '#size' => 10,
    ];
    $form['rootterm']['sloggen_rt_level'] = [
        '#type' => 'textfield',
        '#title' => t('How many level to generate menu terms?'),
        '#description' => t('Maximum number: %max level', ['%max' => SlogGen::RT_LEVEL_MAX]),
        '#default_value' => $rt_level,
        '#size' => 10,
    ];


    $form['menuterm'] = [
        '#type' => 'details',
        '#id' => 'detail-menuterm',
        '#title' => $this->t('Menu term defaults'),
        '#open' => TRUE,
        '#tree' => FALSE,
    ];
    $mt_number = SlogGen::configValue($config, 'sloggen_mt_number', SlogGen::DEFAULT_MT_NUMBER);
    $mt_range = SlogGen::configValue($config, 'sloggen_mt_range', SlogGen::DEFAULT_MT_RANGE);
    $form['menuterm']['sloggen_mt_number'] = [
        '#type' => 'textfield',
        '#title' => t('How many slogitems would you like to generate?'),
        '#description' => t('Maximum number: %max elements', ['%max' => SlogGen::MT_NUMBER_MAX]),
        '#default_value' => $mt_number,
        '#size' => 10,
    ];
    // time range for field 'created' in slogitem table (SlogItem entity)
    $form['menuterm']['sloggen_mt_range'] = [
        '#type' => 'select',
        '#title' => t('How far back in time should the slogitems be dated?'),
        '#description' => t('Element creation dates will be distributed randomly from the current time, back to the selected time.'),
        '#options' => SlogGen::optionsTimeAgo(),
        '#default_value' => $mt_range,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $err = false;
    $names = ['sloggen_rt_number', 'sloggen_rt_level', 'sloggen_mt_number'];
    foreach ($names as $field_name) {
      $val = (integer) $values[$field_name];
      switch ($field_name) {
        case 'sloggen_rt_number':
          $err = ($val < 1 || $val > SlogGen::RT_NUMBER_MAX);
          break;
        case 'sloggen_rt_level':
          $err = ($val < 1 || $val > SlogGen::RT_LEVEL_MAX);
          break;
        case 'sloggen_mt_number':
          $err = ($val < 1 || $val > SlogGen::MT_NUMBER_MAX);
          break;
      }
      if ($err) {
        break;  // leave for
      }
    }
    if ($err) {
      $err_msg = t('Please note the limitations.');
      $form_state->setErrorByName($field_name, $err_msg);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $integrate = (boolean) $values['sloggen_integrate'];
    $config = $this->config('sloggen.settings');
    $config->set('sloggen_integrate', $integrate)
            ->set('sloggen_rt_number', $values['sloggen_rt_number'])
            ->set('sloggen_rt_level', $values['sloggen_rt_level'])
            ->set('sloggen_mt_number', $values['sloggen_mt_number'])
            ->set('sloggen_mt_range', $values['sloggen_mt_range'])
            ->save();
    \Drupal::messenger()->addStatus(t('Settings have been saved.'));
    if ($integrate !== $this->integrate_old) {
      \Drupal::messenger()->addWarning(t('To take affect yuo have to clear all caches.'));
    }
  }

}
