<?php

/**
 * @file
 * Contains \Drupal\sloggen\Form\GenerateMenutermsForm.
 */

namespace Drupal\sloggen\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sloggen\SlogGen;
use Drupal\slogxt\SlogXt;

/**
 * Confirmation form for generating menu terms.
 */
class GenerateMenutermsForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sloggen_generate_menuterms';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Generate menu terms');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $args = ['%path' => $this->rootterm->pathLabel()];
    return $this->t('Generate menu terms for root term: %path', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $args = ['slogtx_rt' => $this->rootterm_id];
    return new Url('entity.slogtx_rt.overview_form', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Generate');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $rootterm = \Drupal::request()->get('slogtx_rt');
    if ($rootterm && $rootterm->isValidTerm()) {
      $this->rootterm = $rootterm;
      $this->rootterm_id = $rootterm->id();
      $config = $this->config('sloggen.settings');

      $form['genopts'] = [
        '#type' => 'details',
        '#id' => 'detail-rootterm',
        '#title' => $this->t('Generate options'),
        '#open' => TRUE,
        '#tree' => FALSE,
      ];

      $rt_number = SlogGen::configValue($config, 'sloggen_rt_number', SlogGen::DEFAULT_RT_NUMBER);
      $rt_level = SlogGen::configValue($config, 'sloggen_rt_level', SlogGen::DEFAULT_RT_LEVEL);
      $form['genopts']['sloggen_rt_number'] = [
        '#type' => 'textfield',
        '#title' => t('How many menu terms would you like to generate per level?'),
        '#description' => t('Maximum number: %max elements', ['%max' => SlogGen::RT_NUMBER_MAX]),
        '#default_value' => $rt_number,
        '#size' => 10,
      ];
      $form['genopts']['sloggen_rt_level'] = [
        '#type' => 'textfield',
        '#title' => t('How many level to generate menu terms?'),
        '#description' => t('Maximum number: %max level', ['%max' => SlogGen::RT_LEVEL_MAX]),
        '#default_value' => $rt_level,
        '#size' => 10,
      ];
//      $form['genopts']['kill'] = [
//        '#type' => 'checkbox',
//        '#title' => t('Clear before generating'),
//        '#description' => t('Deletes all existing elements before generating new.'),
//        '#default_value' => FALSE,
//      ];

      return parent::buildForm($form, $form_state);
    }
    else {
      $path = $rootterm ? $rootterm->pathLabel() : 'undefined';
      $args = ['%path' => $path];
      $msg = $this->t("This is no valid root term. </br>Path: %path.", $args);
      \Drupal::messenger()->addError($msg);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $err = false;
    $names = ['sloggen_rt_number', 'sloggen_rt_level'];
    foreach ($names as $field_name) {
      $val = (integer) $values[$field_name];
      switch ($field_name) {
        case 'sloggen_rt_number':
          $err = ($val < 1 || $val > SlogGen::RT_NUMBER_MAX);
          break;
        case 'sloggen_rt_level':
          $err = ($val < 1 || $val > SlogGen::RT_LEVEL_MAX);
          break;
      }
      if ($err) {
        break;  // leave for
      }
    }
    if ($err) {
      $err_msg = t('Please note the limitations.');
      $form_state->setErrorByName($field_name, $err_msg);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // redirect
    $form_state->setRedirectUrl($this->getCancelUrl());

    $generated = FALSE;
    if (isset($this->rootterm)) {
      $values = $form_state->getValues();
      $data = [
        'vocabulary' => $this->rootterm->getVocabulary(),
        'selected_ids' => [$this->rootterm_id],
        'kill' => (boolean) (isset($values['kill']) ? $values['kill'] : FALSE),
        'number' => $values['sloggen_rt_number'],
        'levels' => $values['sloggen_rt_level'],
      ];
      $generated = SlogGen::generateSlogTerms($data);
    }

    // message/log
    $args = [
      '%path' => isset($this->rootterm) ? $this->rootterm->pathLabel() : 'undefined',
      '%tid' => $this->rootterm_id,
    ];
    if ($generated) {
      $msg = $this->t('Menu terms have been generated for root term: (%tid) %path.', $args);
      \Drupal::messenger()->addStatus($msg);
      SlogXt::logger('sxt_slogitem')->notice($msg);
    }
    else {
      $msg = $this->t('Menu terms have NOT been generated for root term: (%tid) %path.', $args);
      \Drupal::messenger()->addError($msg);
      SlogXt::logger('sxt_slogitem')->error($msg);
    }
  }

}
