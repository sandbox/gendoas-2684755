<?php

/**
 * @file
 * Contains \Drupal\sloggen\Event\SloggenEventSubscriber.
 */

namespace Drupal\sloggen\Event;

use Drupal\slogxt\Event\SlogxtEvents;
use Drupal\user\Entity\Role;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * SlogGenerate event subscriber.
 */
class SloggenEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events[SlogxtEvents::AJAXFORM_RESPONSE_ALTER][] = ['onAjaxFormPreResponse', 10];

    return $events;
  }

  public function onAjaxFormPreResponse(Event $event) {
    $hasPerm = FALSE;
    $perm = 'allow slog generate';
    $roles = Role::loadMultiple(\Drupal::currentUser()->getRoles());
    foreach ($roles as $key => $role) {
      if (in_array($perm, $role->getPermissions())) {
        $hasPerm = TRUE;
        break;
      }
    }

    $data = $event->getData();
    $data['form']['#attached']['drupalSettings']['slogxt']['permissions']['slogtx'][$perm] = $hasPerm;
  }

}
