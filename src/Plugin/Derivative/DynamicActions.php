<?php

/**
 * @file
 * Contains \Drupal\sloggen\Plugin\Derivative\DynamicActions.
 */

namespace Drupal\sloggen\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\sloggen\SlogGen;

/**
 * Defines dynamic actions.
 */
class DynamicActions extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $config = \Drupal::config('sloggen.settings');
    $integrate = (boolean) (SlogGen::configValue($config, 'sloggen_integrate', FALSE));
    if ($integrate && \Drupal::moduleHandler()->moduleExists('slogtx_ui')) {
      // generate terms
      $this->derivatives['sloggen.rootterm'] = $base_plugin_definition;
      $rootterm = & $this->derivatives['sloggen.rootterm'];
      $rootterm['id'] = 'sloggen.action.rootterm';
      $rootterm['title'] = "Generate menu terms";
      $rootterm['route_name'] = 'sloggen.action.menuterms';
      $rootterm['appears_on'] = ['entity.slogtx_rt.overview_form'];

      // generate terms
      $this->derivatives['sloggen.slogitems'] = $base_plugin_definition;
      $slogitems = & $this->derivatives['sloggen.slogitems'];
      $slogitems['id'] = 'sloggen.action.slogitems';
      $slogitems['title'] = "Generate slogitems";
      $slogitems['route_name'] = 'sloggen.action.slogitems';
      $slogitems['appears_on'] = ['entity.slogtx_mt.overview'];
    }

    return $this->derivatives;
  }

}
