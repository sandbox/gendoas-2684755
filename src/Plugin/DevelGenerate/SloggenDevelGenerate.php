<?php

/**
 * @file
 * Contains \Drupal\sloggen\Plugin\DevelGenerate\SloggenDevelGenerate.
 */

namespace Drupal\sloggen\Plugin\DevelGenerate;

use Drupal\sloggen\SlogGen;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a ContentDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "sloggen",
 *   label = @Translation("slog elements"),
 *   description = @Translation("Generate a given number of slogitems. Optionally delete current slogitems."),
 *   url = "sloggen",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num_terms" = 2,
 *     "num_level" = 3,
 *     "num_items" = 10,
 *     "kill" = FALSE,
 *     "title_length" = 2,
 *     "time_range" = 2592000
 *   }
 * )
 */
class SloggenDevelGenerate extends DevelGenerateBase {

  protected $max_num_terms = 20;
  protected $max_num_items = 40;

//  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
//    parent::__construct($configuration, $plugin_id, $plugin_definition);
//  }

  /**
   * Return the form elements for the form devel_generate_form_sloggen.
   * 
   * Entry point, 
   * called by  \Drupal\devel_generate\Form\DevelGenerateForm::buildForm()
   * 
   * @return array
   *  Array of renderable form elements
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $has_module_sxt_slogitem = \Drupal::moduleHandler()->moduleExists('sxt_slogitem');

    // _plugin_id required by ajax call,
    // see getPluginIdFromRequest() in \Drupal\devel_generate\Form\GenerateForm::buildForm
    $form['_plugin_id'] = [
      '#type' => 'hidden',
      '#value' => 'sloggen',
    ];

    $ajax_settings = [
      'callback' => $this->getMyCallback('ajaxOnSelectedVocabulary'),
      'wrapper' => 'terms-select-wrapper',
      'effect' => 'fade',
    ];

    // generate slogitems (sxt_slogitem module) or slogtx terms
    $available = $has_module_sxt_slogitem ? '' : ' (not available)';
    $generate_target = [
      'items' => t('Slogitems') . $available,
      'terms' => t('Slogtx terms'),
    ];
    $form['generate_target'] = [
      '#type' => 'radios',
      '#title' => t('Element type'),
      '#description' => t('Which type of element to generate.'),
      '#options' => $generate_target,
      '#default_value' => $has_module_sxt_slogitem ? 'items' : 'terms',
      '#disabled' => $has_module_sxt_slogitem ? FALSE : TRUE,
      '#ajax' => $ajax_settings,
    ];

    $options = [];
    $vocabularies = SlogTx::sortByPathLabel(SlogTx::getAllVocabularies());
    foreach ($vocabularies as $vid => $vocabulary) {
      $options[$vid] = $vocabulary->pathLabel();
    }
    $form['vocab_id'] = [
      '#type' => 'select',
      '#title' => t('Vocabulary'),
      '#options' => $options,
      '#required' => TRUE,
      '#ajax' => $ajax_settings,
    ];

    $input_values = $form_state->getValues();

    // Select target term, root term and more
    $form['select_more_wrapper'] = $this->settingsFormSelectTerms($input_values);


    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => t('<strong>Delete all elements</strong> before generating new elements.'),
      '#description' => t('There will be deleted elements for selections only.'),
      '#default_value' => $this->getSetting('kill'),
    ];

    $form['#validate'][] = $this->getMyCallback('validate');

    return $form;
  }

  private function settingsFormSelectTargetTerm(&$values, $vocabulary) {
    $tt_storage = SlogTx::entityStorage('slogtx_tt');
    $target_tids = $tt_storage->getLevelTermIds($vocabulary->id(), [0]);  // default limit=100
    $terms = $tt_storage->loadMultiple($target_tids);
    $target_term = $values['target_term'] ?: FALSE;
    $term_keys = array_keys($terms);
    if (count($term_keys) == 1) {
      $values['target_term'] = $term_keys[0];
    }
    elseif (!empty($target_term) && !in_array($target_term, $term_keys)) {
      unset($values['target_term']);
    }

    if (empty($terms)) {
      $args = ['%vocab' => $vocabulary->pathLabel()];
      $msg = t('There are no target terms in vocabulary %vocab. Add target terms ', $args);
      $elements['select_empty'] = SlogXt::xtRenderMessage($msg, 'warning');
    }
    else {
      $terms_options = [];
      foreach ($terms as $tid => $term) {
        $terms_options[$tid] = $term->pathLabel();
      }

      $ajax_settings = [
        'callback' => $this->getMyCallback('ajaxOnSelectedVocabulary'),
        'wrapper' => 'terms-select-wrapper',
        'effect' => 'fade',
      ];

      $default = isset($values['target_term']) ? $values['target_term'] : NULL;
      $element = [
        '#type' => 'select',
        '#options' => $terms_options,
        '#title' => t('Select a target term'),
        '#description' => t('Select a target term for which to generate elements.'),
        '#default_value' => $default,
        '#required' => TRUE,
        '#ajax' => $ajax_settings,
      ];
    }

    return $element;
  }

  /**
   * Return the form elements for selecting root term
   * 
   * Depending of generate target:
   *  - Multiple select for generating slogtx terms
   *  - Single select for generating slogitems
   * 
   * @param array $values 
   *  Input data of the form
   * @return array
   *  Array of renderable form elements
   */
  private function settingsFormSelectTerms($values) {
    $elements = [
      '#type' => 'fieldset',
      '#title' => t('Extended'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#attributes' => [
        'id' => 'terms-select-wrapper',
      ],
    ];

    $has_vocabulary = FALSE;
    $has_target_term = FALSE;
    $has_empty_target_term = FALSE;
    if (!empty($values['generate_target']) && !empty($values['vocab_id'])) {
      $vid = $values['vocab_id'];
      $vocabulary = SlogTx::getVocabulary($vid);
      $has_vocabulary = !empty($vocabulary);
    }


    // Select target term
    if ($has_vocabulary) {
      $elements['target_term'] = $this->settingsFormSelectTargetTerm($values, $vocabulary);
      $has_empty_target_term = isset($elements['target_term']['select_empty']);
      $has_target_term = !empty($values['target_term']);
    }



    if ($has_vocabulary && $has_target_term) {
      $generate_terms = ($values['generate_target'] == 'terms');
      $generate_items = !$generate_terms;

      $vocabulary = SlogTx::getVocabulary($values['vocab_id']);
      $target_term = $vocabulary->getTargetTerm($values['target_term']);
      $terms = $target_term->getRootTerms(TRUE);
      $terms_options = [];
      foreach ($terms as $tid => $term) {
        $terms_options[$tid] = $term->pathLabel();
      }

      if (empty($terms_options)) {
        $args = ['%vocab' => $vocabulary->pathLabel()];
        $msg = t('There are no root terms in vocabulary %vocab.', $args);
        $elements['select_empty'] = SlogXt::xtRenderMessage($msg, 'warning');
        //
        // we are ready now,
        // return the empty element
        //
        return $elements;
      }


      //
      // it is ensured, there are root terms,
      // so we can serve more selections
      // 
      $ajax_settings_items = [
        'callback' => $this->getMyCallback('ajaxOnSelectedRootTerm'),
        'wrapper' => 'slog-term-select-wrapper',
        'effect' => 'fade',
      ];

      if ($generate_terms) {    // generate terms
        $elements['root_terms'] = [
          '#type' => 'checkboxes',
          '#options' => $terms_options,
          '#title' => t('Select root terms'),
          '#description' => t('Check root terms for which to generate elements. All root terms will be used if omitted.'),
        ];
        $elements['num_terms'] = [
          '#type' => 'textfield',
          '#title' => t('How many menu terms would you like to generate per level?'),
          '#description' => t('Maximum number: %max elements', ['%max' => $this->max_num_terms]),
          '#default_value' => $this->getSetting('num_terms'),
          '#size' => 10,
        ];
        $elements['num_level'] = [
          '#type' => 'textfield',
          '#title' => t('How many level to generate menu terms?'),
          '#description' => t('Maximum number: %max level', ['%max' => 3]),
          '#default_value' => $this->getSetting('num_level'),
          '#size' => 10,
        ];
        $elements['num_items'] = ['#type' => 'hidden', '#value' => $this->getSetting('num_items')];
      }
      else {    // generate slogitems
        // SlogItem entity is defined in sxt_slogitem module
        $elements['root_term'] = [
          '#type' => 'select',
          '#options' => $terms_options,
          '#title' => t('Select a root term'),
          '#description' => t('Select a root terms for which to generate elements.'),
          '#required' => TRUE,
          '#ajax' => $ajax_settings_items,
        ];
        // select slogtx terms for which to generate slogitems
        $elements['items_more_wrapper'] = $this->settingsFormSelectSlogTerms($values);

        $elements['node_type'] = [
          '#type' => 'select',
          '#title' => t('What node type to use'),
          '#options' => SlogGen::optionsNodeTypes(),
          '#description' => t('You may decide to use only nodes of a certain type. This has no effect for redirects.'),
          '#default_value' => '0',
        ];


        $elements['num_items'] = [
          '#type' => 'textfield',
          '#title' => t('How many slogitem elements would you like to generate?'),
          '#description' => t('Maximum number: %max', ['%max' => $this->max_num_items]),
          '#default_value' => $this->getSetting('num_items'),
          '#size' => 10,
        ];
        // time range for field 'created' in slogitem table (SlogItem entity)
        $elements['time_range'] = [
          '#type' => 'select',
          '#title' => t('How far back in time should the slogitems be dated?'),
          '#description' => t('Element creation dates will be distributed randomly from the current time, back to the selected time.'),
          '#options' => SlogGen::optionsTimeAgo(),
          '#default_value' => $this->getSetting('time_range'),
        ];

        $elements['num_terms'] = ['#type' => 'hidden', '#value' => $this->getSetting('num_terms')];

        if ($vocabulary->getToolbar()->isSysToolbar()) {
          $elements['redirect_elements'] = [
            '#type' => 'checkbox',
            '#title' => t('Redirect elements.'),
            '#description' => t('Create slogitem redirect element, which redirect to other slogitem elements with node target.'),
            '#default_value' => TRUE,
          ];
        }
      }
    }
    // 
    // no more elements since there is no vocabulary selected
    //
    elseif (!$has_empty_target_term) {
      $msg = t('Ensure vocabulary and target term are selected.');
      $elements['select_empty'] = SlogXt::xtRenderMessage($msg, 'warning');
    }

    return $elements;
  }

  /**
   * Return the form elements for selecting slogtx terms
   * 
   *  - Applies for generating slogitems only
   *  - Therefore module sxt_slogitem has to be enabled
   *  - Multiple select for generating slogitems
   * 
   * @param array $values 
   *  Input data of the form
   * @return array
   *  Array of renderable form elements
   */
  private function settingsFormSelectSlogTerms($values) {
    $vocabulary = SlogTx::getVocabulary($values['vocab_id']);
    $elements = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'slog-term-select-wrapper',
      ],
    ];

    $root_term_id = $values['root_term'] ?: FALSE;
    if (!empty($root_term_id)) {
      $term = SlogTx::getRootTerm($root_term_id);
      $path_label = $term->pathLabel();
      $tree = $vocabulary->getVocabularyMenuTree($term->id(), 3, TRUE);

      if (!empty($tree)) {
        $parents = [];
        foreach ($tree as $item) {
          if (isset($item->parents[0])) {
            $parents[] = $item->parents[0];
          }
        }

        $options = [];
        $prefix = ['', '--.', '--.--.'];
        foreach ($tree as $item) {
          $options[$item->id()] = $prefix[$item->depth] . $item->label();
        }

        if (!empty($options)) {
          $elements['slogterms'] = [
            '#type' => 'checkboxes',
            '#options' => $options,
            '#title' => t('Select menu terms'),
            '#description' => t('Check terms for which to generate slogitems. All terms will be used if omitted.'),
          ];
        }
        else {
          $msg = t('Unknown error for %path_label.', ['%path_label' => $path_label]);
          $elements['slogterms'] = SlogXt::xtRenderMessage($msg, 'error');
        }
      }
      // empty term tree
      else {
        $msg = t('No terms found for %path_label.', ['%path_label' => $path_label]);
        $elements['slogterms'] = SlogXt::xtRenderMessage($msg, 'warning');
      }
    }
    // root term not selected
    else {
      $msg = t('Ensure root term is selected.');
      $elements['rootterm_empty'] = SlogXt::xtRenderMessage($msg, 'warning');
    }

    return $elements;
  }

  /**
   * Return form element for ajax replacement: after vocabulary selection
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array 
   *  Return the sector, which is to be replaced.
   */
  public static function ajaxOnSelectedVocabulary($form, FormStateInterface $form_state) {
    return $form['select_more_wrapper'];
  }

  /**
   * Return form element for ajax replacement: after root term(s) selection
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return array 
   *  Return the sector, which is to be replaced.
   */
  public static function ajaxOnSelectedRootTerm($form, FormStateInterface $form_state) {
    return $form['select_more_wrapper']['items_more_wrapper'];
  }

  /**
   * Callbach function for validation
   * 
   * - Do not validate for ajax call
   * - Required selection of vocabulary, target term and root term(s)
   * - For vocabulary target entity the existence of the entity is validated
   * 
   * @see self::settingsForm()
   * 
   * @param array $form
   * @param FormStateInterface $form_state
   * @return NULL
   */
  public static function validate($form, FormStateInterface $form_state) {
    if (!empty(\Drupal::request()->get('_drupal_ajax'))) {
      // on ajax request there is no need for validating
      return;
    }

    // prepare generate data for validation only
    $values = $form_state->getValues();
    if (!empty($values['target_term'])) {
      $vocabulary = SlogTx::getVocabulary($values['vocab_id']);
      $plugin = $vocabulary->getTargetEntityPlugin();
      if (!$plugin->isValidTargetTermId($values['target_term'], $err_msg)) {
        $form_state->setErrorByName('target_term', $err_msg);
      }
    }
  }

  public static function prepareGenerateData($values, $validate_only) {
    $vocabulary = SlogTx::getVocabulary($values['vocab_id']);
    if ($vocabulary) {
      
    }
    $generate_data = ['vocabulary' => $vocabulary];

    if ($validate_only) {
      return $generate_data;
    }

    // 
    // prepare more data for generating elements
    //
    
    $generate_terms = ($values['generate_target'] == 'terms');
    $generate_items = !$generate_terms;

    if ($generate_terms) {
      // prepare selected RootTerms
      $root_term_ids_selected = array_filter($values['root_terms']);
      if (empty($root_term_ids_selected)) {
        $root_term_ids_selected = $values['root_terms'];
      }
      $root_term_ids_selected = array_keys($root_term_ids_selected);
      $generate_data['root_term_ids_selected'] = $root_term_ids_selected;

      // 
      $selected_vid = $values['vocab_id'];
      $parent = 0;
      // retrieve term ids from the selected vocabulary
      $root_terms = $vocabulary->getRootTerms(TRUE);
      $selected = [];
      foreach ($root_terms as $tid => $root_term) {
        if (in_array($tid, $root_term_ids_selected)) {
          $selected[$tid] = $root_term;
        }
      }
      $generate_data['root_terms'] = $selected;
      $generate_data['rootterm_tids'] = array_keys($selected);
    }
    // generate items
    else {
      $generate_data['root_term'] = $values['root_term'];
      $generate_data['time_range'] = $values['time_range'];

      $slogterms = array_filter($values['slogterms']) ?: $values['slogterms'];
      $generate_data['slogterms'] = array_keys($slogterms);
    }


    return $generate_data;
  }

  /**
   * {@inheritdoc}
   * 
   * Overries \Drupal\devel_generate\DevelGenerateBase::generateElements()
   */
  protected function generateElements(array $values) {
    $generate_data = self::prepareGenerateData($values, false);
    $generate_terms = ($values['generate_target'] == 'terms');
    $generate_items = !$generate_terms;

    if ($generate_terms) {
      $values['num_terms'] = (integer) $values['num_terms'];
      if ($values['num_terms'] <= $this->max_num_terms) {
        $data = [
          'vocabulary' => $generate_data['vocabulary'],
          'selected_ids' => $generate_data['root_term_ids_selected'],
          'kill' => (boolean) $values['kill'],
          'number' => $values['num_terms'],
          'levels' => $values['num_level'],
        ];
        SlogGen::generateSlogTerms($data);
      }
      else {
        $this->generateBatchSlogTerms($values, $generate_data);
      }
    }
    else {
      $values['num_items'] = (integer) $values['num_items'];
      if ($values['num_items'] <= $this->max_num_items) {
        $data = [
          'selected_ids' => $generate_data['slogterms'],
          'kill' => (boolean) $values['kill'],
          'node_type' => $values['node_type'],
          'number' => $values['num_items'],
          'range' => $generate_data['time_range'],
          'redirect_elements' => $values['redirect_elements'] ?: FALSE,
        ];
        SlogGen::generateSlogItems($data);
      }
      else {
        $this->generateBatchSlogItems($values, $generate_data);
      }
    }
  }

  private function generateBatchSlogTerms($values, $generate_data) {
    // not supported
    $xx = 0;
  }

  private function generateBatchSlogItems($values, $generate_data) {
    // not supported
    $xx = 0;
  }

  /**
   * {@inheritdoc}
   */
  public function validateDrushParams($args) {
    // not supported
    return [];
  }

  /**
   * Privat helper function
   */
  private function getMyCallback($method) {
    $class = '\\' . get_class($this);
    return "$class::$method";
  }

}
