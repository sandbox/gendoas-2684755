<?php

/**
 * @file
 * Definition of Drupal\sloggen\Plugin\slogxt\edit\main\GenerateMenuitems.
 */

namespace Drupal\sloggen\Plugin\slogxt\edit\main;

use Drupal\sloggen\SlogGen;
use Drupal\slogtx\Interfaces\TxTermInterface;
use Drupal\slogtb\Plugin\slogxt\edit\main\TbPluginEditBase;

/**
 * 
 * @SlogxtEdit(
 *   id = "sloggen_menuitems_generate",
 *   bundle = "main",
 *   title = @Translation("dev: Generate menu items"),
 *   description = @Translation("Create some menu items in a selected toolbar item."),
 *   route_name = "sloggen.main.edit.menuitems.generate",
 *   resolve_base_command = "slogtb::resolvePathTbTabId",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "allow slog generate",
 *     "slogtx" = "allow slog generate"
 *   },
 *   skipable = false,
 *   weight = 99999
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class GenerateMenuitems extends TbPluginEditBase {

  protected function getResolveArgs() {
    return [
      'perms' => $this->getPermissions(),
      '{base_entity_id}' => [
        'xtInfo' => t('Select the toolbar item for which to generate menu items.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasActionItemsForSysTab(TxTermInterface $root_term) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function isActionExecutable() {
    return ($this->hasUserTbMenu()  //
        || $this->hasRoleTbPerm('allow slog generate') //
        || $this->hasGlobalTbAdmin() //
        );
  }
  
  /**
   * {@inheritdoc}
   */
  protected function access() {
      $config = \Drupal::config('sloggen.settings');
      if (SlogGen::configValue($config, 'sloggen_integrate', FALSE)) {
        return parent::access();
      }
    
    return FALSE;
  }

}
