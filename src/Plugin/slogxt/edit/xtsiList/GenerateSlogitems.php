<?php

/**
 * @file
 * Definition of Drupal\sloggen\Plugin\slogxt\edit\xtsiList\GenerateSlogitems.
 */

namespace Drupal\sloggen\Plugin\slogxt\edit\xtsiList;

use Drupal\sloggen\SlogGen;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * 
 * @SlogxtEdit(
 *   id = "sloggen_slogitems_generate",
 *   bundle = "xtsi_liedit",
 *   title = @Translation("dev: Generate list items"),
 *   description = @Translation("Create some list items with dummy contents."),
 *   route_name = "sloggen.xtsi.edit.listitems.generate",
 *   permissions = {
 *     "user" = TRUE,
 *     "role" = "allow slog generate",
 *     "slogtx" = "allow slog generate"
 *   },
 *   skipable = false,
 *   weight = 99999
 * )
 * 
 * @see \Drupal\slogxt\Annotation\SlogxtEdit
 */
class GenerateSlogitems extends XtPluginEditBase {

  protected function access() {
    $user_permissions = SlogXt::getUserPermissions();
    if ($user_permissions === TRUE) {
      return TRUE;
    }

    $access = FALSE;
    if ($this->account->isAuthenticated() && \Drupal::moduleHandler()->moduleExists('sxt_slogitem')) {
      $config = \Drupal::config('sloggen.settings');
      $access = (boolean) (SlogGen::configValue($config, 'sloggen_integrate', FALSE));
      if ($access) {
        if (SlogXt::isSuperUser($this->account)) {
          return TRUE;
        }

        $menu_tid = $this->get('baseEntityId');
        $vocabulary = SlogTx::getMenuTerm($menu_tid)->getVocabulary();
        if (!$vocabulary->isUnderscoreToolbar()) {
          $toolbar_id = $vocabulary->getToolbarId();
          $checkPermToolbar = TRUE;
          if ($toolbar_id !== 'role') {
            $access = FALSE;
            $checkPermToolbar = in_array('allow slog generate', $user_permissions);
          }

          if ($checkPermToolbar) {
            $access = SlogXt::hasPermToolbar($toolbar_id, $this->getPermissions());
          }
        }
      }
    }

    return $access;
  }

}
