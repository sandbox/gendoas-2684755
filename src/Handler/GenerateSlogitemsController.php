<?php

/**
 * @file
 * Definition of \Drupal\sloggen\Handler\GenerateSlogitemsController.
 */

namespace Drupal\sloggen\Handler;

use Drupal\sxt_slogitem\SlogXtsi;
use Drupal\sxt_slogitem\XtsiNodeTypeData;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

class GenerateSlogitemsController extends XtEditControllerBase {

  protected $menuTerm;
  protected $opSave = false;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return ('\Drupal\sloggen\Form\GenerateSlogitemsForm');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $title = $this->menuTerm ? $this->menuTerm->label() : '???';
    return t('Generate list items for %title', ['%title' => $title]);
  }

  protected function getSubmitLabel() {
    return $this->t('Generate');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::hookFormAlter();
   */
  public function hookFormAlter(&$form, FormStateInterface $form_state, $form_id) {
    $gopts_type = & $form['genopts']['node_type'];
    if (!empty($gopts_type)) {
      $types = XtsiNodeTypeData::getXtNodeTypes();
      $xt_types = [];
      foreach ($types as $type_id => $type) {
        $xt_types[$type_id] = $type->label();
      }
      $gopts_type['#options'] = $xt_types;
    }
    
    $controllerClass = get_class($this);
    $form['#validate'][] = "$controllerClass::formValidate";
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $request = \Drupal::request();
    $this->menuTerm = $request->get('base_entity_id');
    if (!$this->menuTerm || !$this->menuTerm->isMenuTerm()) {
      $message = t('Menu term not found.');
      throw new \LogicException($message);
    }

    // 
    $request->attributes->set('slogtx_mt', $this->menuTerm);
    $request->attributes->remove('base_entity_id');

    // 
    $op = $request->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $form['genopts']['#attributes']['class'][] = 'slogxt-input-field';

    $allowed = ['sloggen_mt_number', 'redirect_elements', 'node_type', 'kill'];
    foreach (Element::children($form['genopts']) as $field_name) {
      if (!in_array($field_name, $allowed)) {
        $form['genopts'][$field_name]['#access'] = false;
      }
    }

    unset($form['actions']['cancel']);
    if ($submit = &$form['actions']['submit']) {
      $submit['#value'] = t('Save');
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      }
      else {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    }
    else {
      $slogxtData['wizardFinalize'] = true;
    }
  }
  
  /**
   * Add submit handler.
   */
  public static function formValidate(array &$form, FormStateInterface $form_state) {
    $slogxt_data = & $form_state->get('slogxt');
    $slogxt_data['sids_old'] = SlogXtsi::getSidsFromTids([$slogxt_data['menu_tid']]);
    
    $submit_handlers = &$form_state->getTriggeringElement()['#submit'];
    if (empty($submit_handlers)) {
      $submit_handlers = $form['#submit'];
    }
    $submit_handlers[] = [get_class(), 'formSubmit'];
    $form_state->setSubmitHandlers($submit_handlers);
  }

  /**
   * Prepare done slogitem data
   */
  public static function formSubmit(array &$form, FormStateInterface $form_state) {
    $done_sids = [];
    $slogxt_data = & $form_state->get('slogxt');
    $sids = SlogXtsi::getSidsFromTidsSorted([$slogxt_data['menu_tid']]);
    $sid_new = array_values(array_diff($sids, $slogxt_data['sids_old']));
    if (!empty($sid_new) && is_array($sid_new) && $slogitem = SlogXtsi::loadSlogitem($sid_new[0])) {
      $done_sids[$slogitem->id()] = $slogitem->getAttachData();
    }
    
    $slogxt_data['done_sids'] = $done_sids;
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();
    $data = $this->slogxt_data_final;
    return [
      'command' => 'sxt_slogitem::finishedSlogitemNewItems',
      'args' => $data['done_sids'],
    ];
  }

}
