<?php

/**
 * @file
 * Definition of \Drupal\sloggen\Handler\GenerateMenuitemsController.
 */

namespace Drupal\sloggen\Handler;

use Drupal\slogtx\SlogTx;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Controller\XtEditControllerBase;

class GenerateMenuitemsController extends XtEditControllerBase {

  protected $rootTerm;
  protected $opSave = false;

  /**
   * Implements \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormObjectArg();
   */
  protected function getFormObjectArg() {
    return ('\Drupal\sloggen\Form\GenerateMenutermsForm');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getFormTitle();
   */
  protected function getFormTitle() {
    $vocabulary = $this->rootTerm->getVocabulary();
    return t('Generate menu items for %title', ['%title' => $vocabulary->getTbtabLabel()]);
  }

  protected function getSubmitLabel() {
    return $this->t('Generate');
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::buildContentResult();
   */
  protected function buildContentResult(&$form, FormStateInterface $form_state) {
    $slogxtData = &$form_state->get('slogxtData');
    $slogxtData['runCommand'] = 'wizardFormEdit';
    return parent::buildContentResult($form, $form_state);
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::preBuildForm();
   */
  protected function preBuildForm(FormStateInterface $form_state) {
    parent::preBuildForm($form_state);

    $request = \Drupal::request();
    $this->rootTerm = $request->get('base_entity_id');
    if (!$this->rootTerm || !$this->rootTerm->isRootTerm()) {
      $message = t('Root term not found.');
      throw new \LogicException($message);
    }

    // 
    $request->attributes->set('slogtx_rt', $this->rootTerm);
    $request->attributes->remove('base_entity_id');

    // 
    $op = $request->get('op', false);
    if ($op) {
      $this->opSave = ($op === (string) t('Save'));
    }
  }

  /**
   * Overwrite to alter form before rendering.
   * 
   * @param type $form
   * @param type $form_state
   */
  protected function preRenderForm(&$form, FormStateInterface $form_state) {
    parent::preRenderForm($form, $form_state);
    $form['genopts']['#attributes']['class'][] = 'slogxt-input-field';

    unset($form['actions']['cancel']);
    if ($submit = &$form['actions']['submit']) {
      $submit['#value'] = t('Save');
    }

    $slogxtData = & $form_state->get('slogxtData');
    if ($this->opSave) {
      $hasErrors = $form_state->hasAnyErrors();
      if ($hasErrors) {
        $this->formResult = 'edit';
      }
      else {
        $slogxtData['wizardFinished'] = true;
        $slogxtData['onWizardFinished'] = $this->getOnWizardFinished();
      }
    }
    else {
      $slogxtData['wizardFinalize'] = true;
    }
  }

  /**
   * Overrides \Drupal\slogxt\Controller\AjaxFormControllerBase::getOnWizardFinished();
   */
  protected function getOnWizardFinished() {
    $this->setSavedMessage();

    $rootTerm = $this->rootTerm;
    if ($rootTerm) {
      $vocabulary = $rootTerm->getVocabulary();
      list($toolbar, $toolbartab) = SlogTx::getVocabularyIdParts($vocabulary->id());
      return [
        'command' => 'slogtb::finishedTbMenuAdd',
        'args' => [
          'tid' => $rootTerm->id(),
          'name' => $rootTerm->label(),
          'description' => $rootTerm->getDescription(),
          'toolbar' => $toolbar,
          'toolbartab' => $toolbartab,
        ],
      ];
    }

    return FALSE;
  }

}
