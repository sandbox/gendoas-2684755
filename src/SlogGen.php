<?php

/**
 * @file
 * Contains \Drupal\sloggen\SlogGen.
 */

namespace Drupal\sloggen;

use Drupal\Component\Utility\Random;
use Drupal\slogtx\SlogTx;
use Drupal\slogxt\SlogXt;
use Drupal\slogtx\Entity\MenuTerm;
use Drupal\sxt_slogitem\Entity\SlogItem;

/**
 * Statics for sloggen module.
 */
class SlogGen {

  // root term
  const DEFAULT_RT_NUMBER = 4;
  const DEFAULT_RT_LEVEL = 3;
  const RT_NUMBER_MAX = 20;
  const RT_LEVEL_MAX = 3;
  // menu term defaults
  const DEFAULT_MT_NUMBER = 10;
  const DEFAULT_MT_RANGE = 2592000;
  const DEFAULT_MT_STATUS = 99;
  const MT_NUMBER_MAX = 40;
  const MT_STATUS_MAX = 100;

  /**
   * The random data generator.
   *
   * @var \Drupal\Component\Utility\Random 
   */
  protected static $random = FALSE;

  /**
   * Return options for time ago range.
   * 
   * Items from 'now' to '1 year ago'. 
   * 
   * @return array 
   *  Randerable array for a select box.
   */
  public static function optionsTimeAgo() {
    $formatter = \Drupal::service('date.formatter');
    $options = [1 => t('Now')];
    foreach ([3600, 86400, 604800, 2592000, 31536000] as $interval) {
      $options[$interval] = $formatter->formatInterval($interval, 1) . ' ' . t('ago');
    }
    return $options;
  }

  /**
   * Return a config value or default if not found.
   * 
   * @param \Drupal\Core\Config\Config $config
   * @param string $key
   * @param mixed $default Optional, defaults to NULL.
   * @return mixed
   */
  public static function configValue($config, $key, $default = NULL) {
    $result = $config->get($key);
    return isset($result) ? $result : $default;
  }

  /**
   * Returns the random data generator.
   *
   * @return \Drupal\Component\Utility\Random
   *   The random data generator.
   */
  public static function getRandom() {
    if (!self::$random) {
      self::$random = new Random();
    }
    return self::$random;
  }

  public static function optionsNodeTypes($add_all = TRUE) {
    $options = $add_all ? ['- All node types -'] : [];
    return ($options += node_type_get_names());
  }

  /**
   * Generate slogtx menu terms for a set of root terms.
   * 
   * @param array $data 
   *  Data needed for generation.
   * @return boolean
   *  TRUE on success
   */
  public static function generateSlogTerms($data) {
    try {
      $selected_ids = $data['selected_ids'];
      if (is_array($selected_ids) && !empty($selected_ids)) {
        if ($data['kill']) {
          self::killSlogTerms($selected_ids);
        }

        $vocabulary = $data['vocabulary'];
        $number = (integer) $data['number'];
        $levels = (integer) $data['levels'];
        foreach ($selected_ids as $tid) {
          self::doGenerateSlogTerms($vocabulary, $tid, $number, $levels);
        }
        $generated = TRUE;
      }
      else {
        SlogXt::logger('sloggen')->error('Generate menu terms failed: unexpected');
        $generated = FALSE;
      }
    }
    catch (\RuntimeException $e) {
      SlogXt::logger('sloggen')->error('Generate menu terms failed:</br>' . $e->getMessage());
      $generated = FALSE;
    }

    return $generated;
  }

  /**
   * Recursive function for generating terms and child terms.
   */
  public static function doGenerateSlogTerms($vocabulary, $root_tid, $number, $levels = 3) {
    $levels = ($levels > 3 || $levels < 1) ? 3 : $levels;
    $vid = $vocabulary->id();
    $lancode = $vocabulary->get('langcode');
    $weight = -1001;
    $random = self::getRandom();
    for ($i = 0; $i < $number; $i++) {
      $name = $random->sentences(random_int(2, 6), TRUE);
      $values = [
          'vid' => $vid,
          'name' => $name,
          'description' => $random->sentences(random_int(4, 20)),
          'parent' => $root_tid,
          'langcode' => $lancode,
          'weight' => $weight,
      ];
      $term = MenuTerm::create($values);
      $term->save();

      $levels--;
      if ($weight < -1000 && $levels > 0 && is_object($term)) {
        self::doGenerateSlogTerms($vocabulary, $term->id(), $number, $levels);
      }
      $weight = random_int(-1000, 1000);
    }
  }

  /**
   * Delete all child terms of all selected root terms
   * 
   * @param array $root_term_ids
   *  Array of root term ids.
   */
  public static function killSlogTerms(array $root_term_ids) {
    $storage = SlogTx::entityStorage('slogtx_rt');
    foreach ($storage->loadMultiple($root_term_ids) as $term) {
      $term->delete();
    }
  }

  /**
   * Generate slogitems for a set of menu terms.
   * 
   * @param array $data 
   *  Data needed for generation.
   * @return boolean
   *  TRUE on success
   */
  public static function generateSlogItems($data) {
    if ($data['redirect_elements']) {
      $entity_type = 'slogitem';
      $route_name = 'entity.slogitem.redirect';
      $target_IDs = self::getAvailableSlogitemIds();
      if ($data['kill']) {
        $target_IDs = array_values(array_diff($target_IDs, $data['selected_ids']));
      }
      $target_count = count($target_IDs);
    }
    else {
      $entity_type = 'node';
      $route_name = 'entity.node.canonical';
      $target_IDs = self::getAvailableNodeIds($data['node_type']);
      $target_count = count($target_IDs);
    }
    if ($target_count < 100) {
      \Drupal::messenger()->addWarning('Not enough elements available for generating slogitems (required 100).');
      if ($data['redirect_elements']) {
        \Drupal::messenger()->addWarning('Note: redirects to slogitem with node target are used.');
      }
      else {
        \Drupal::messenger()->addWarning('Note: not all node types are suitable.');
      }
      $generated = FALSE;
    }
    else {
      try {
        $selected_ids = $data['selected_ids'];
        if (is_array($selected_ids) && !empty($selected_ids)) {
          if ($data['kill']) {
            self::killSlogItems($selected_ids);
          }

          $number = (integer) $data['number'];
          $range = (integer) $data['range'];

          foreach ($selected_ids as $tid) {
            $added = [];
            for ($i = 0; $i < $number; $i++) {
              $target_idx = random_int(0, $target_count - 1);
              $k = 0; // prevent endless loop
              while (in_array($target_idx, $added)) {
                $target_idx = random_int(0, $target_count - 1);
                if (++$k > 20) {
                  break;
                }
              }

              $slogitem = SlogItem::create([
                          'tid' => $tid,
                          'title' => self::getRandom()->sentences(random_int(2, 6), TRUE),
                          'status' => TRUE,
                          'weight' => random_int(-1000, 1000),
                          'route_name' => $route_name,
                          'route_parameters' => [$entity_type => $target_IDs[$target_idx]],
                          'entity' => $entity_type,
                          'eid' => $target_IDs[$target_idx],
                          'created' => REQUEST_TIME - random_int(0, $range),
              ]);
              $slogitem->save();
              $added[] = $target_idx;
            }
          }
          $generated = TRUE;
        }
        else {
          SlogXt::logger('sloggen')->error('Generate menu terms failed: unexpected');
          $generated = FALSE;
        }
      }
      catch (\RuntimeException $e) {
        SlogXt::logger('sloggen')->error('Generate menu terms failed:</br>' . $e->getMessage());
        $generated = FALSE;
      }
    }

    return $generated;
  }

  /**
   * Return node IDs usable for generating slogitems.
   * 
   * @return array
   *  An array of node IDs.
   */
  public static function getAvailableNodeIds($node_type = NULL) {
    if (empty($node_type)) {
      $nids = \Drupal::entityQuery('node')->execute();
    }
    else {
      $nids = \Drupal::entityQuery('node')
              ->condition('type', $node_type)
              ->execute();
    }
    return array_values($nids);
  }

  public static function getRandomUserIds($number = 0, $exclude_uids = []) {
    $number = (integer) $number > 0 ? (integer) $number : 100;
    $exclude_uids[] = 0;
    $storage = \Drupal::entityTypeManager()->getStorage('user');
    $result = $storage->getQuery()
            ->condition('uid', $exclude_uids, 'NOT IN')
            ->sort('uuid')
            ->range(0, 1000)
            ->execute();

    return array_rand($result, $number);
  }

  /**
   * Return slogitem IDs usable for generating redirect slogitems.
   * 
   * @return array
   *  An array of slogitem IDs.
   */
  public static function getAvailableSlogitemIds() {
    $query = \Drupal::database()->select('slogitem', 'si');
    $query->join('taxonomy_term_data', 'td', 'td.tid = si.tid');
    $ids = $query
            ->fields('si', ['sid'])
            ->condition('entity', 'node')
            ->range(0, 1000)
            ->execute()
            ->fetchCol();
    return $ids;
  }

  /**
   * Delete all slogitems for all given term ids.
   * 
   * @param array $tids
   */
  public static function killSlogItems($tids) {
    $storage = \Drupal::entityTypeManager()->getStorage('slogitem');
    foreach ($tids as $tid) {
      $storage->deleteByTid($tid);
    }
  }

}
