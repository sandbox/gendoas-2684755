<?php

/**
 * @file
 * Contains \Drupal\sloggen\Routing\SloggenRoutes.
 */

namespace Drupal\sloggen\Routing;

use Drupal\slogxt\SlogXt;
use Symfony\Component\Routing\Route;
use Drupal\slogxt\Routing\SlogxtRoutesBase;

/**
 * Defines a route subscriber to register a url for serving image styles.
 */
class SloggenRoutes extends SlogxtRoutesBase {

  /**
   * Returns an array of route objects.
   *
   * @return \Symfony\Component\Routing\Route[]
   *   An array of route objects.
   */
  public function routes() {
    $baseAjaxPath = trim(SlogXt::getBaseAjaxPath('sloggen'), '/');
    $routes = [];
    $raw_data = [];

    $raw_data += $this->_rawAjaxEdit($baseAjaxPath);

    $defaults = [
      'requirements' => ['_access' => 'TRUE'],
      'options' => [],
      'host' => '',
      'schemes' => [],
      'methods' => [],
      'condition' => null,
    ];

    foreach ($raw_data as $key => $data) {
      $data += $defaults;
      $routes["sloggen.$key"] = new Route(
          $data['path'], $data['defaults'], $data['requirements'], $data['options'], $data['host'], $data['schemes'], $data['methods'], $data['condition']
      );
    }

    return $routes;
  }

  private function _rawAjaxEdit($baseAjaxPath) {
    $base_entity_id = '{base_entity_id}';
    $controllerPath = '\Drupal\sloggen\Handler';

    $raw_data = [
      'main.edit.menuitems.generate' => [
        'path' => "/$baseAjaxPath/tbmenu/$base_entity_id/generate",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateMenuitemsController::getContentResult",
        ],
        'options' => self::$optAjaxBase + [
      'parameters' => [
        'base_entity_id' => ['type' => 'entity:slogtx_rt'],
      ],
        ],
      ],
      'xtsi.edit.listitems.generate' => [
        'path' => "/$baseAjaxPath/listitem/$base_entity_id/generate",
        'defaults' => [
          '_controller' => "{$controllerPath}\GenerateSlogitemsController::getContentResult",
        ],
        'options' => self::$optAjaxBase + [
      'parameters' => [
        'base_entity_id' => ['type' => 'entity:slogtx_mt'],
      ],
        ],
      ],
    ];

    return $raw_data;
  }

}
